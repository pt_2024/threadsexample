package Synchronization;

class Sender {
    public synchronized void send(String msg)
    {
        System.out.println("Sending " + msg);
        try {
            Thread.sleep(1000);
        }
        catch (Exception e) {
            System.out.println("Thread interrupted.");
        }
        System.out.println(msg + "Sent" + "\n");
    }
}

class ThreadedSend extends Thread {
    private String msg;
    Sender sender;

    ThreadedSend(String m, Sender obj)
    {
        msg = m;
        sender = obj;
    }

    public void run()
    {
        msg = msg + " ! ";
        //synchronized (sender){
            sender.send(msg);
        //}
    }
}

class SyncDemo {
    public static void main(String args[])
    {
        Sender send= new Sender();
        ThreadedSend s1 = new ThreadedSend("Hi ", send);
        ThreadedSend s2 = new ThreadedSend("Bye ", send);

        s1.start();
        s2.start();

        try {
            s1.join();
            s2.join();
        }
        catch (Exception e) {
            System.out.println("Interrupted");
        }
    }
}

