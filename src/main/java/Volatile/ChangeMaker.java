package Volatile;

import java.util.logging.Level;

import static Volatile.VolatileTest.LOGGER;
import static Volatile.VolatileTest.MY_INT;

class ChangeMaker implements Runnable {

    @Override
    public void run() {
        int local_value = MY_INT;
        while (MY_INT < 5) {
            LOGGER.log(Level.INFO,
                    "Incrementing MY_INT to {0}",
                    local_value + 1);
            MY_INT = ++local_value;
            try {
                Thread.sleep(500);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
