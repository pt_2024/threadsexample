package Volatile;

import java.util.logging.Logger;


public class VolatileTest {
    static final Logger LOGGER
            = Logger.getLogger("Volatile.VolatileTest");
    static volatile int MY_INT = 0;

    public static void main(String[] args) {
        ChangeListener changeListener = new ChangeListener();
        changeListener.start();

        ChangeMaker changeMaker = new ChangeMaker();
        Thread thread = new Thread(changeMaker);
        thread.start();
    }

}